<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;

class HomeController extends FOSRestController
{
    /**
     * @Get("/")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $response = [
            'status' => 'ok',
            'version' => '0.1'
        ];

        return $this->handleView($this->view($response, 200));
    }
}