<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Artist;
use AppBundle\Form\ArtistType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ArtistController extends FOSRestController
{
    /*
     * General methods
     */

    /**
     * Returns the repository for the Artist entity.
     * @return \AppBundle\Repository\ArtistRepository
     */
    public function getRepository()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Artist');
    }

    /**
     * Returns the Doctrine manager.
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    public function getManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Returns the form associated with the Artist entity.
     * @param null $artist
     * @param null $routeName
     * @param array $routeParams
     * @param null $method
     * @return \Symfony\Component\Form\Form
     */
    protected function getForm($artist = null, $routeName = null, $routeParams = [], $method = null)
    {
        $options = array();
        if (null !== $routeName) {
            $options['action'] = $this->generateUrl($routeName, $routeParams);
        }
        if ($method != null) {
            $options['method'] = $method;
        }
        if ($artist == null) {
            $artist = new Artist();
        }

        return $this->createForm(ArtistType::class, $artist, $options);
    }

    /*
     * REST Actions
     */

    /**
     * Get all the Artists.
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getArtistsAction()
    {
        $artists = $this->getRepository()->findAll();
        $view = $this->view($artists, 200);

        return $this->handleView($view);
    }

    /**
     * Get the form for creating an Artist.
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newArtistsAction()
    {
        $form = $this->getForm(null, 'post_artists');
        $view = $this->view([
            'form' => $form
        ]);
        return $this->handleView($view);
    }

    /**
     * Get a specific Artist by its id.
     * @param $artistId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getArtistAction($artistId)
    {
        $artist = $this->getDoctrine()->getRepository('AppBundle:Artist')->find($artistId);
        if ($artist == null) {
            throw new HttpException(404);
        }
        $view = $this->view($artist, 200);

        return $this->handleView($view);
    }

    /**
     * Post a new Artist.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postArtistsAction(Request $request)
    {
        $form = $this->getForm(null, 'post_artists');

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $manager = $this->getManager();
            $artist = $form->getData();
            $manager->persist($artist);
            $manager->flush();
            return $this->handleView($this->view($artist));
        }
        return $this->handleView($this->view([
            'form' => $form
        ]));
    }

    /**
     * Get the form for editing an artist.
     * @param $artistId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editArtistAction($artistId)
    {
        $artist = $this->getRepository()->find($artistId);
        if ($artist == null) {
            throw new HttpException(404);
        }
        $form = $this->getForm($artist, 'put_artist', ['artistId' => $artistId]);
        $view = $this->view([
            'form' => $form
        ]);
        return $this->handleView($view);
    }

    /**
     * Update a specific Artist by its id.
     * @param Request $request
     * @param $artistId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putArtistAction(Request $request, $artistId)
    {
        $artist = $this->getRepository()->find($artistId);
        if ($artist == null) {
            throw new HttpException(404);
        }
        $form = $this->getForm($artist, 'put_artist', ['artistId' => $artistId], 'PUT');

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $manager = $this->getManager();
            $artist = $form->getData();
            $manager->persist($artist);
            $manager->flush();
            return $this->handleView($this->view($artist));
        }
        return $this->handleView($this->view([
            'form' => $form
        ]));
    }

    /**
     * Delete a specific Artist by its id.
     * @param $artistId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteArtistAction($artistId)
    {
        $artist = $this->getDoctrine()->getRepository('AppBundle:Artist')->find($artistId);
        if ($artist == null) {
            throw new HttpException(404);
        }
        $manager = $this->getManager();
        $manager->remove($artist);
        $manager->flush();

        return $this->handleView($this->view([
            'status' => 'success',
            'deleted' => $artistId
        ]));
    }
}