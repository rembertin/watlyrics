<?php

/**
 * Created by PhpStorm.
 * User: rembertin
 * Date: 15/01/17
 * Time: 15:10
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Artist;
use AppBundle\Entity\Lyric;
use AppBundle\Entity\Song;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LyricFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var Song $song */
        $song = $this->getReference('song');

        $lyricOriginal = new Lyric();
        $lyricOriginal->setSong($song);
        $lyricOriginal->setLanguage('en');
        $lyricOriginal->setText("I'm on the Highway to Hell, on the Highway to Hell!");
        $lyricOriginal->setIsOriginal(true);

        $lyricTranslation = new Lyric();
        $lyricTranslation->setSong($song);
        $lyricTranslation->setLanguage('fr');
        $lyricTranslation->setText("Je suis sur l'Autoroute de l'enfer, sur l'Autoroute de l'enfer !");
        $lyricTranslation->setIsOriginal(false);

        $manager->persist($lyricOriginal);
        $manager->persist($lyricTranslation);
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}