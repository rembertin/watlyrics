<?php

/**
 * Created by PhpStorm.
 * User: rembertin
 * Date: 15/01/17
 * Time: 15:10
 */
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Artist;

class ArtistFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $artist = new Artist();
        $artist->setName('AC/DC');
        $artist->setBiography("AC/DC's biography");

        $manager->persist($artist);
        $manager->flush();

        $this->addReference('artist', $artist);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 0;
    }
}