<?php

/**
 * Created by PhpStorm.
 * User: rembertin
 * Date: 15/01/17
 * Time: 15:10
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Artist;
use AppBundle\Entity\Song;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SongFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $song = new Song();

        /** @var Artist $artist */
        $artist = $this->getReference('artist');
        $song->setArtist($artist);
        $song->setTitle("Highway To Hell");
        $song->getPublishYear();

        $manager->persist($song);
        $manager->flush();

        $this->addReference('song', $song);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}