<?php

namespace AppBundle\Tests;

use AppBundle\DataFixtures\ORM\ArtistFixture;
use AppBundle\DataFixtures\ORM\LyricFixture;
use AppBundle\DataFixtures\ORM\SongFixture;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiTest extends WebTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\Client
     */
    protected $client;

    /**
     * @var null|\Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var object|\Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    protected $router;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    protected $repository;

    /**
     * @var ORMExecutor
     */
    private $fixtureExecutor;

    /**
     * @var ContainerAwareLoader
     */
    private $fixtureLoader;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected $em;

    function __construct()
    {
        parent::__construct();
        static::bootKernel();
        $this->client = static::createClient();
        $this->container = $this->client->getContainer();
        $this->router = $this->container->get('router');
        $this->em = $this->container->get('doctrine')->getManager();
        $this->repository = $this->container->get('doctrine')->getRepository('AppBundle:Artist');
    }

    protected function setUp()
    {
        $this->addFixture(new ArtistFixture());
        $this->addFixture(new SongFixture());
        $this->addFixture(new LyricFixture());
        $this->executeFixtures();
    }

    /**
     * Adds a new fixture to be loaded.
     *
     * @param FixtureInterface $fixture
     */
    protected function addFixture(FixtureInterface $fixture)
    {
        $this->getFixtureLoader()->addFixture($fixture);
    }

    /**
     * Executes all the fixtures that have been loaded so far.
     */
    protected function executeFixtures()
    {
        $this->getFixtureExecutor()->execute($this->getFixtureLoader()->getFixtures());
    }

    /**
     * @return ORMExecutor
     */
    private function getFixtureExecutor()
    {
        if (!$this->fixtureExecutor) {
            $this->fixtureExecutor = new ORMExecutor($this->em, new ORMPurger($this->em));
        }
        return $this->fixtureExecutor;
    }

    /**
     * @return ContainerAwareLoader
     */
    private function getFixtureLoader()
    {
        if (!$this->fixtureLoader) {
            $this->fixtureLoader = new ContainerAwareLoader($this->container);
        }
        return $this->fixtureLoader;
    }

    /**
     * @param $method
     * @param $route
     * @param array $routeParams
     * @return mixed
     */
    protected function sendRequestToRoute($method, $route, array $routeParams = [])
    {
        $this->client->request($method, $this->router->generate($route, $routeParams));
        return $this->client->getResponse()->getContent();
    }
}
