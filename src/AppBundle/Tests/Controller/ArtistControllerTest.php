<?php

namespace AppBundle\Tests\Controller;

use AppBundle\Entity\Artist;
use AppBundle\Form\ArtistType;
use AppBundle\Tests\ApiTest;

class ArtistControllerTest extends ApiTest
{
    public function testGetArtists()
    {
        $contentDecoded = json_decode($this->sendRequestToRoute('GET', 'get_artists', [
            '_format' => 'json'
        ]), true);
        $this->assertCount(1, $contentDecoded);
        $firstItem = $contentDecoded[0];
        $this->assertArraySubset([
            'name' => 'AC/DC',
            'biography' => "AC/DC's biography",
        ], $firstItem);
    }

    public function testGetArtist()
    {
        $contentDecoded = json_decode($this->sendRequestToRoute('GET', 'get_artist', [
            'artistId' => 1,
            '_format' => 'json'
        ]), true);
        $this->assertArraySubset([
            'name' => 'AC/DC',
            'biography' => "AC/DC's biography",
        ], $contentDecoded);
    }

    public function testPostArtists()
    {
        $route = $this->router->generate('post_artists', ['_format' => 'json']);
        $form = $this->container->get('form.factory')
            ->create(ArtistType::class, new Artist());
        $dataSended = [
            $form->getName() => [
                'name' => 'Iron Maiden',
                'biography' => 'The best metal band ever.'
            ]
        ];
        $this->client->request('POST', $route, $dataSended, [], ['HTTP_Accept' => 'application/json']);
        $response = $this->client->getResponse();

        $this->assertArraySubset($dataSended[$form->getName()], json_decode($response->getContent(), true));

        $artistsCount = count($this->repository->findAll());
        $this->assertEquals(2, $artistsCount);
    }

    public function testPutArtist()
    {
        $artistId = 1;
        $route = $this->router->generate('put_artist', [
            'artistId' => $artistId,
            '_format' => 'json'
        ]);

        $form = $this->container->get('form.factory')
            ->create(ArtistType::class, new Artist());
        $dataSended = [
            $form->getName() => [
                'name' => 'AC-DC updated',
                'biography' => 'YEAAAAHHHHH'
            ]
        ];
        $this->client->request('PUT', $route, $dataSended, [], ['HTTP_Accept' => 'application/json']);
        $response = $this->client->getResponse();

        $this->assertArraySubset($dataSended[$form->getName()], json_decode($response->getContent(), true));
    }

    public function testDeleteArtist()
    {
        $artistId = 1;
        $route = $this->router->generate('delete_artist', [
            'artistId' => $artistId,
            '_format' => 'json'
        ]);

        $this->client->request('DELETE', $route);
        $response = $this->client->getResponse();

        $this->assertArraySubset([
            'status' => 'success',
            'deleted' => $artistId
        ], json_decode($response->getContent(), true));

        $deletedArtist = $this->repository->find($artistId);
        self::assertNull($deletedArtist);
    }
}