<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lyric
 *
 * @ORM\Table(name="lyric")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LyricRepository")
 */
class Lyric
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=255)
     */
    private $language;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_original", type="boolean")
     */
    private $isOriginal;

    /**
     * @ORM\ManyToOne(targetEntity="Song", inversedBy="lyrics")
     * @ORM\JoinColumn(name="song_id", referencedColumnName="id")
     */
    private $song;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Lyric
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Lyric
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set isOriginal
     *
     * @param boolean $isOriginal
     *
     * @return Lyric
     */
    public function setIsOriginal($isOriginal)
    {
        $this->isOriginal = $isOriginal;

        return $this;
    }

    /**
     * Get isOriginal
     *
     * @return bool
     */
    public function getIsOriginal()
    {
        return $this->isOriginal;
    }

    /**
     * @return Song
     */
    public function getSong()
    {
        return $this->song;
    }

    /**
     * @param Song $song
     */
    public function setSong($song)
    {
        $this->song = $song;
    }
}

