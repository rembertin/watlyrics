WatLyrics
======

A RESTFul API allowing to manage some songs lyrics and song translations.

It was built with [Symfony 3](https://symfony.com/) and uses [FOSRestBundle](https://github.com/FriendsOfSymfony/FOSRestBundle).

##Contribute

If you wish you can contribute to this project, please follow these steps:

- Make sure your local webserver matches the [Symfony 3 requirements](http://symfony.com/doc/current/reference/requirements.html)
- Execute the commands:

```shell
$ git clone https://rembertin@bitbucket.org/rembertin/watlyrics.git watlyrics
$ cd watlyrics
$ composer install
$ php bin/console server:start
```
- Then setup the database and run `php bin/console doctrine:fixtures:load` 
- Finally, run `php bin/console server:start` and let it go!

## Tests
To ensure everything is all right, you can run the tests  via this command (from the project base directory):
```shell
$ phpunit --configuration phpunit.xml.dist
```